Installation via docker
-----------------------

Pour les systèmes Linux, une `image
docker <https://cloud.docker.com/u/lbraud/repository/docker/lbraud/climap>`_
est disponible ainsi qu'un `serveur nginx
configuré <https://cloud.docker.com/repository/docker/lbraud/mynginx>`_.

Il n'y pas besoin de récupérer directement ces images. Lorsque `docker et
docker-compose sont installés <https://docs.docker.com/install/>`_, il
suffit de récupérer `le fichier docker-compose.yml
<https://gitlab.com/laurent.braud/geoportail/blob/observations/docker-compose.yml>`_
et d'entrer les commandes suivantes pour l'installation. ::

$ docker-compose up

Cela lancera le téléchargement des images (~2Go) et lancera le
serveur, qui sera disponible sur le port 80 (HTML) de la machine.


Initialisation
^^^^^^^^^^^^^^

La première fois que le système est lancé, il est absolument
vide. Ouvrir un autre terminal, se placer dans le répertoire du
``docker-compose.yml`` et entrer ::

  $ docker-compose run web init/install.sh

.. NOTE:: Pour reinitialiser toutes les données, le plus simple est de
   supprimer tous les volumes docker et reprendre l'installation. Éteindre
   le service, puis entrer ::

     $ docker volume rm $(docker volume ls -q)

   et reprendre l'initialisation.
   

Mise en place d'un volume extérieur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour importer des données extérieures (notamment fichiers ``netcdf``
et ``csv``), il faut modifier une ligne dans le
``docker-compose.yml``::
  
  web:
    volumes:
      - /path/to/my/data/:/data/


.. DANGER::

   Le volume doit être en place *avant* le lancement du container
   docker, sinon celui-ci créera lui-même le dossier, ce qui peut
   poser problème dans le cas des dossiers temporaires (par exemple :
   clef usb qui crée un dossier dans ``/media/login/``).  Dans ce cas,
   démonter la clef, éteindre le service, supprimer le répertoire
   vide, puis refaire l'opération : brancher la clef *avant* de
   rallumer le service.

Accès au système
^^^^^^^^^^^^^^^^

Pour se connecter au container docker en "terminal", avoir accès au système de fichier, etc::

  $ docker-compose run web /bin/bash

Pour entrer directement dans le shell python::
  
  $ docker-compose run web python manage.py shell


