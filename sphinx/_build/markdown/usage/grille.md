# Données grillées et/ou régionales

Le géoportail contient deux types de données : les *métriques*, qui
sont précalculées au pas de temps mensuel ou annuel (*e.g.* « nombre de
jours de pluie »), et les *paramètres* bruts, au format journalier
(*e.g.* « précipitation journalière »), qui permettent à l’utilisateur
d’entrer manuellement une formule de calcul.

Ces données sont disponibles pour les dimensions spatiales suivantes :
la *grille* (« pixel », ou raster), la *zone* (région sous forme de
polygone), le *point* (station météo).

|  | métriques

 | paramètres

 |
|  | --------- | ---------- |
| **grille**

 | oui

       |            |
| **région**

 | oui

       | oui

        |
| **station**

 | calculé à la volée

 | oui

        |
Ce chapitre se penche sur les métriques et les paramètres des données
grillées et régionales, qui sont calculées en même temps. Pour les données
station, voir la section Correction de données stations.

## Métriques

### Calcul

Les données doivent être calculées indépendamment, à l’aide d’outils
tels que [cdo](https://code.mpimet.mpg.de/projects/cdo/) ou [nco](http://nco.sourceforge.net/nco.html) à partir des paramètres
journaliers des modèles **déjà corrigés**. Cette opération peut donc
se faire sur une machine différente du géoportail.

Par exemple, si l’on dispose de précipitations journalières dans
`/netcdf/daily/pr/`, on peut calculer le *nombre de jours de pluie >
30mm* avec la commande

```
mkdir -p /netcdf/metrics/prdays20
cd /netcdf/daily/pr/
for f in *.nc; do
  cdo chname,pr,prdays20 -monsum -gec,20 $f ../../metrics/prdays20/$f
done
```

Dans l’ordre inverse : la commande `gec` vérifie que la valeur est
supérieur à 20, `monsum` fait la somme de ces jours, `chname`
assigne un nouveau nom à cette variable.

### Insertion

Après le calcul, il doit y avoir un fichier netcdf par modèle et par
scénario, et ils seront tous stockés selon le même format. On
appellera **motif** le schéma général dans lequel le nom du modèle et
celui du scénario sont respectivement remplacés par `{model}` et
`{scenario}`.

Par exemple, si les fichiers sont de la forme
`/netcdf/tasmax/rcp85/tasmax_IPSL-CM5A-LR.nc`, le motif sera
`/netcdf/tasmax/{scenario}/tasmax_{model}.nc`.

La métrique calculée et tous les modèles doivent déjà apparaître dans
la base de données. Dans un shell python, pour une métrique *tasmax*,
on aurait les lignes suivantes :

```
from proj import load, models as pm
metric = pm.Metric.objects.get(shortname='tasmax')
load.metric_from_nc(metric, file_pattern='/netcdf/tasmax/{scenario}/tasmax_{model}.nc')
```

Sans autre option, la fonction s’occupera de polygoniser la grille
selon la forme des régions, et chargera également les fichiers netcdf
dans la base de donnée. Il est possible de faire seulement l’un ou
l’autre. Consulter `help(load.metric_from_nc)` pour toutes les
options.

**NOTE**: Un autre moyen de calculer les métriques est de les calculer
à partir des paramètres journaliers déjà entrés. Par
exemple, calculer le *cumul* à partir de la formule `pr`
en aggrégeant par `sum`. Il suffit de définir la métrique
ainsi puis d’appeler la fonction suivante dans le shell
python:

```
from proj import load, models as pm
my_metric = pm.Metric.objects.get(shortname='my metric shortname')
load.metric_from_formula(my_metric)
```

Cette méthode est plus rapide, mais ne produit pas de
données grille, et toutes les métriques ne peuvent être
calculées de cette façon (dryspell, par exemple).

Enfin, les résultats peuvent différer de ceux produits par
l’autre méthode, étant donné que les deux opérations
(application de la formule / moyenne par zone) ne sont sont
pas commutatives.

## Paramètres bruts

Les paramètres journaliers (des modèles **corrigés**) peuvent être
importées pour les régions à partir des fichiers netcdf. Pour des
raisons de quantité de données et de temps de calcul, les données
raster ne sont pas stockées telles quelles dans le géoportail.

```
from proj import load, models as pm
my_param = pm.Parameter.objects.get(shortname='my parameter shortname')
load.parameter_from_nc(my_param, file_pattern='/path/to/netcdf/{scenario}/param_{model}.nc')
```

**NOTE**: Si les données régionales ont déjà été calculées par
ailleurs (sous forme de fichier csv), les paramètres peuvent
être importés plus rapidement avec la fonction

```
load.parameter_from_csv(my_param, file_pattern='/netcdf/{zone}_{scenario}.csv')
```

Comme toujours, help(load.parameter_from_csv) affiche plus
de détails.

### Correction des paramètres grillés

*Travail à réaliser.*
