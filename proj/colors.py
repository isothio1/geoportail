"""
Map coloring util.
"""
from matplotlib.colors import LinearSegmentedColormap, Normalize

OUTOFBOUNDS_COLOR = (50,50,50)

def make_colors(code,array,vmin=None,vmax=None):
    """Associates a color (as RGB tuple) to each value in array."""
    vmin, vmax = vmin or min(array), vmax or max(array)
    # two-time code split
    codesplit = code.format(vmin=vmin, vmax=vmax).split('\r\n')
    codesplit2 = [ r.split(',') for r in codesplit ]
    # value evaluation
    rows = [
        (eval(row[0]), int(row[1]), int(row[2]), int(row[3]))
        for row in codesplit2 ]

    res,n = [], len(rows)-1
    for x in array:
        if x > rows[0][0] or x < rows[-1][0]:
            res.append(OUTOFBOUNDS_COLOR)
            continue
        for i in range(n):
            if rows[i][0] >= x >= rows[i+1][0]:
                if rows[i][0] == rows[i+1][0] :
                    vali = 0
                else:
                    vali = (rows[i][0]-x)/(rows[i][0]-rows[i+1][0])
                res.append(tuple((
                    rows[i][rgb]*(1-vali)+rows[i+1][rgb]*vali
                    for rgb in range(1,4))))
                break
    return res

def tuple2hex(color):
    """Translates a triplet into hex form for html use.
    In case of RGBA the Alpha channel is dropped."""
    return '#{:02x}{:02x}{:02x}'.format(*(map(int, color[:3])))

import numpy as np

class Coloring:
    """Either array or vmin/vmax must be provided at init."""

    def __init__(self, code, array=None, vmin=None, vmax=None):
        self.vmin, self.vmax = vmin, vmax
        if vmin is None:
            self.vmin = min(array)
        if vmax is None:
            self.vmax = max(array)

        # two-time code split
        codesplit = code.format(vmin=self.vmin, vmax=self.vmax).split('\r\n')
        codesplit = [ row.split(',') for row in codesplit ]
        codesplit = [ [eval(row[0]), int(row[1])/255, int(row[2])/255, int(row[3])/255]  for row in codesplit ]

        self.vmin = max(self.vmin, codesplit[-1][0])
        self.vmax = min(self.vmax, codesplit[0][0])

        # extraction part (way too long)
        self.rows, i = [], 0
        while codesplit[i][0] >= self.vmax:
            i += 1
        if codesplit[i][0] != self.vmax:
            vali = (codesplit[i][0]-self.vmax)/(codesplit[i][0]-codesplit[i-1][0])
            self.rows.append([1, [codesplit[i][rgb]*(1-vali)+codesplit[i-1][rgb]*vali for rgb in range(1,4)]])
        while codesplit[i][0] > self.vmin:
            self.rows.append( [ ((codesplit[i][0]-self.vmin)/(self.vmax-self.vmin)), [ codesplit[i][rgb] for rgb in range(1,4)]])
            i += 1

        vali = (codesplit[i][0]-self.vmin)/(codesplit[i][0]-codesplit[i-1][0])
        self.rows.append([0, [codesplit[i][rgb] * (1 - vali) + codesplit[i - 1][rgb] * vali for rgb in range(1, 4)]])

        # build the matplotlib colormap
        self.rows.reverse()
        # DEBUG
        # print(self.rows, self.vmin, self.vmax)

        self.mpl = LinearSegmentedColormap.from_list('tmp', self.rows)
        # bad values are transparent
        self.mpl.set_under(alpha=0)
        self.mpl.set_over(alpha=0)
        self.mpl.set_bad(alpha=0)
        self.norm = Normalize(self.vmin, self.vmax)

    def make_scale(self, ashex=False, log=True):
        if log:
            a = np.geomspace(self.vmin+1, self.vmax-1, 9)
        else:
            a = np.linspace(self.vmin, self.vmax, 9)
        return a.tolist(), self.eval(a, ashex=ashex)

    def eval(self, a, alpha=True, ashex=False):
        res = self.mpl(self.norm(a), bytes=True, alpha=None)
        if not alpha:
            # remove alpha channel
            res = res.T[:3].T
        if ashex:
            return list(map(tuple2hex, res))
        return res

    def __call__(self, *args, **kwargs):
        return self.eval(*args, **kwargs)

    # cmapdict = {
    #     'red': [],
    #     'green': [],
    #     'blue': [],
    # }
    # for i in range(9):
    #     cmapdict['red'].append((i / 8, scalecols[i][0] / 255, scalecols[i][0] / 255))
    #     cmapdict['green'].append((i / 8, scalecols[i][1] / 255, scalecols[i][1] / 255))
    #     cmapdict['blue'].append((i / 8, scalecols[i][2] / 255, scalecols[i][2] / 255))
    # # what cost ?
    # cmap = LinearSegmentedColormap('temp', cmapdict)
