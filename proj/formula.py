"""
Utility functions and constants to use in formula.
"""

# allowed words in formula
KEYWORDS = { 'count', 'abs', 'sum', 'max', 'min', 'quantile', 'consec', 'consec2' }

def words(f):
    return ''.join(map(lambda x:x if x.isalpha() else ' ', f)).split()

def variables(f):
    v = set()
    for w in words(f):
        if w not in KEYWORDS:
            v.add(w)
    return v

# see also
# df = df[(df.index.month>=6)&(df.index.month<=10)]
# res = (df<=3).resample('Y').agg(lambda d:consec(d).max())

def consec(prop):
    # returns dataframe of indexes in current interval
    # correct formula found on
    # https://stackoverflow.com/questions/27626542/counting-consecutive-positive-value-in-python-array
    return prop * (prop.groupby((prop != prop.shift()).cumsum()).cumcount() + 1)

def consec2(prop, n):
    #
    inter = consec(prop)
    return (inter > 0) & (inter % n == 0)

CONTEXT = {
    '__builtins__': None,
    'consec': consec,
    'consec2': consec2,
    'abs': abs,
}
