from django.contrib import admin
from django.utils.html import format_html
from django.shortcuts import reverse
from . import models as pm

# Register your models here.

admin.site.register(pm.Zone)
# admin.site.register(pm.Place)
admin.site.register(pm.Scenario)
admin.site.register(pm.ClimateModel)
# admin.site.register(pm.Metric)
admin.site.register(pm.MetricCategory)
# admin.site.register(pm.DailyRegionData)
admin.site.register(pm.Parameter)
admin.site.register(pm.Station)

@admin.register(pm.Place)
class PlaceAdmin(admin.ModelAdmin):
    search_fields = ['name']

@admin.register(pm.Metric)
class MetricAdmin(admin.ModelAdmin):
    list_display = [ 'locale_name', 'data_view' ]

    def data_view(self, obj):
        return format_html('<a class="button" href="{}">data view</a>', reverse('proj:print_zd')+'?metric={}'.format(obj.id))



@admin.register(pm.ColorMap)
class DivisionAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('name', 'name_fr', 'custom')
        }),
        ('Code', {
            'fields': ('code', 'diffcode'),
            'description': "Color codes for map display. Each line is of the form 'X, R, G, B' where "
                           "(*) R,G,B are the Red, Green and Blue values, "
                           "(*) X is an arithmetic expression which may contain {vmin} and {vmax}, e.g. '{vmin}*.3' ."
                           "Lines must be ordered by decreasing value of X.",
        }),
    )

# admin.site.register(pm.ColorMap)

