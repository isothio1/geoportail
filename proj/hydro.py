"""TEMPORARY FILE"""

# TODO : merge this into load

import os
from .load import NCDFData as NCDFRaster
from . import models as pm

ncdir = '/home/laurent/netcdf/hydro/'
# file = ncdir+'Senegal_{model}_mmflow_out.nc'
file = ncdir+'{model}_senegal_filtered.nc'
param = 'mmflow'

newfile = '/home/laurent/netcdf/metrics/runoff/mon_calc_ACCESS1-0_rcp85_runoff.nc'

def make_example():
    di = {'name': 'runoff',
          'name_fr': 'débit',
          'shortname': 'mmflow',
          'category_id': 2,
          'unit': 'm3/s',
          'color_id': 11,
          'description': 'EXPERIMENTAL.',
          'aggregation': 'sum',
          'monthly': True,
          'formula': '',
          'raster_only': True}
    runoff = pm.Metric(*di)
    runoff.save()
    populate()


def test_load():
    pass

def populate(delete_all=False):
    runoff = pm.Metric.objects.get(name='runoff')
    scen = pm.Scenario.objects.get(name='rcp85')
    if delete_all:
        pm.MonthlyRasterData.objects.filter(metric=runoff).delete()
        # HydroTest.objects.all().delete()

    htsts,i = [], 0
    # TODO : scens & yrs
    for cm in pm.ClimateModel.objects.all():
        cmfile = file.format(model=cm.name)
        if os.path.exists(cmfile):
            print(cmfile)
            r = NCDFRaster(cmfile, param=runoff.shortname, lon='Longitude', lat='Latitude')
            for y in range(2006, 2100):
                # htsts = []
                for m in range(1,13):
                    htsts.append(pm.MonthlyRasterData(year=y, month=m,raster=r.to_gdal(i),
                                                      metric=runoff, scenario=scen, model=cm))
                    i += 1
            pm.MonthlyRasterData.objects.bulk_create(htsts)


