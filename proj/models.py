"""
We assume the following nomenclature :
 - a 'Metric' is a monthly pre-calculated measure
    has data as
    - ZonedData (hdf5-based), for zones
    - MonthlyRasterData (postgis raster), for pixels
  Metrics are ordered in MetricCategories.

 - a 'Parameter' is a basic climatic variable in daily form
    has data as
    - DailyRegionData (hdf5-based), for zones,

 - a 'StationParam' is a parameter for Station.
"""

ncfile_path=None

import pandas as pd

from django.contrib.gis.db import models
from django.forms.models import model_to_dict
from django.contrib.gis.geos import Point
from django.utils.translation import get_language
from django.conf import settings
from django_pandas.managers import DataFrameManager
import os

class NamedModel(models.Model):
    """Abstract class for everything named."""
    name = models.CharField(max_length=100)
    name_fr = models.CharField(max_length=100, default="", blank=True)

    @property
    def locale_name(self):
        l = get_language()
        if l and l[:2] == 'fr' and self.name_fr :
            return self.name_fr
        return self.name


    def __str__(self):
        return self.locale_name

    @property
    def capital_name(self):
        return self.locale_name.capitalize()


    class Meta:
        ordering = ('name',)
        abstract = True

class ClimateModel(NamedModel):
    description = models.TextField(blank=True, max_length = 500)
    url = models.URLField(blank=True)
    # todo : https://github.com/jaredly/django-colorfield
    color = models.CharField(max_length=20, help_text='Color in graphs.', default='blue')
    STYLE_CHOICES = ('solid', 'dashed', 'dotted', 'dashdot')
    style = models.CharField(choices=zip(STYLE_CHOICES, STYLE_CHOICES), default='solid', max_length=max(map(len,STYLE_CHOICES)))

    def set_random_color(self):
        # set same color for all submodels of the same model :
        # color is a hex value in [0, 2**24-1]
        # main model is the first substring of the name, which we hash into a color
        self.color = '#' + hex(hash(self.name.split('-')[0]) % 2**24)[2:]
        self.save()

class Scenario(NamedModel):
    public_name = models.CharField(max_length=40)
    public_name_fr = models.CharField(max_length=40)
    color = models.CharField(max_length=20, help_text='Color in graphs.')

    @property
    def locale_public_name(self):
        l = get_language()
        if l and l[:2] == 'fr' and self.name_fr:
            return self.public_name_fr
        return self.public_name


class Zone(NamedModel):
    mpoly = models.MultiPolygonField()

class Place(NamedModel):
    """Named point, typically a city."""
    lonlat = models.PointField(null=True, blank=True)
    lon = models.FloatField(null=True)
    lat = models.FloatField(null=True)
    show = models.BooleanField(blank=True)

    def save(self, *args, **kwargs):
        if self.lon and self.lat:
            self.lonlat = Point(self.lon, self.lat)
        super(Place, self).save(*args, **kwargs)


class ColorMap(NamedModel):
    code = models.TextField(help_text='For raw data display.')
    diffcode = models.TextField(help_text='For difference display. Zero should bear the value 255,255,255 (white).')
    custom  = models.BooleanField(default=False, blank=True, help_text='Available for custom formulae ?')

class MetricCategory(NamedModel):
    public = models.BooleanField(default=False, blank=True)

    class Meta:
        verbose_name_plural = "Metric categories"

    def get_ordered_metrics(self):
        """For display in templates."""
        return self.metric_set.all().order_by('name')


AGGREGATION_TYPES = ('sum', 'mean', 'max', 'min', 'count')

class Metric(NamedModel):
    class Meta:
        ordering = ('category', 'shortname', 'name')

    shortname = models.CharField(max_length=20, help_text="For export and ordering, and netcdf import.", null=True, blank=True, unique=True)
    category = models.ForeignKey(MetricCategory, null=True, on_delete=models.CASCADE)
    unit = models.CharField(max_length=20)
    color = models.ForeignKey(ColorMap, null=True, on_delete=models.SET_NULL)
    description = models.TextField(blank=True)
    aggregation = models.CharField(choices=zip(AGGREGATION_TYPES,AGGREGATION_TYPES), default='sum', max_length=5, help_text='For aggregating years. For instance yearly maximum temperature is obtained by taking the "max" of monthly temperature.')
    monthly = models.BooleanField(default=True, blank=True, help_text='As opposed to yearly data.')
    formula = models.CharField(max_length=100, blank=True, default='', help_text='For calculating from station values.')
    raster_only = models.BooleanField(default=False, blank=True, help_text='There is no regional information.')
    is_log = models.BooleanField(default=False, blank=True, help_text='Log scale ?')


    def to_dict(self):
        return model_to_dict(self)

    def to_dict_safe(self):
        return model_to_dict(self, exclude=['monthly', 'shortname'])

    @property
    def actual_models(self):
        zd = self.zoneddata_set.first()
        res = {}
        with pd.HDFStore(zd.file) as store:
            for s in Scenario.objects.all():
                df = store.select(s.name, stop=0)
                res[s.name] = list(df.columns)
        return res

    @property
    def actual_nc_models(self):
        res = {}
        for s in Scenario.objects.all():
            res[s.name] = list(MonthlyRasterData.objects.filter(metric=self, scenario=s).values_list('model__name', flat=True).distinct())
        return res

class ZonedData(models.Model):
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)
    metric = models.ForeignKey(Metric, on_delete=models.CASCADE)
    file = models.FilePathField(path=os.path.join(settings.BASE_DIR, 'hdf5', 'zonedmetrics'), recursive=True)

    def get_data(self, key):
        with pd.HDFStore(self.file, mode='r', driver='H5FD_CORE') as store:
            return store[key]



class MonthlyRasterData(models.Model):
    """Raster data for pixel requests."""
    class Meta:
        indexes = [
            models.Index(fields=['year']),
            models.Index(fields=['scenario']),
            models.Index(fields=['model']),
            models.Index(fields=['metric']),
        ]

    model = models.ForeignKey(ClimateModel, on_delete=models.CASCADE)
    scenario = models.ForeignKey(Scenario, on_delete=models.CASCADE)
    metric = models.ForeignKey(Metric, on_delete=models.CASCADE)
    year = models.IntegerField()
    month = models.IntegerField()
    raster = models.RasterField()
    objects = DataFrameManager()

    def __str__(self):
        return '(raster) {} {} {}, {}/{}'.format(self.metric, self.scenario, self.model, self.month, self.year)


## Daily parameters, for custom formula use

class Parameter(NamedModel):
    agg = models.CharField(choices=zip(AGGREGATION_TYPES, AGGREGATION_TYPES),
                           verbose_name='Formule d\'aggrégation', max_length=10, default='sum')
    color = models.CharField(max_length=10)
    shortname = models.CharField(max_length=10, unique=True)

    def save(self, *args, **kwargs):
        os.makedirs(os.path.join(settings.BASE_DIR, 'hdf5', 'stationproj', self.shortname), exist_ok=True)
        os.makedirs(os.path.join(settings.BASE_DIR, 'hdf5', 'debiased', self.shortname), exist_ok=True)
        super(NamedModel, self).save(*args, **kwargs)

    def nc_lonlat(self, sc_name, lon, lat, model_ids=None):
        if model_ids:
            qset = self.netcdfdata_set.filter(model__in=model_ids, scen__name=sc_name).order_by('model')
        else:
            qset = self.netcdfdata_set.filter(scen__name=sc_name).order_by('model')
        l = [ ncd.lonlat(lon, lat) for ncd in qset ]
        return pd.concat(l, axis=1)

class DailyRegionData(models.Model):
    param = models.ForeignKey(Parameter, on_delete=models.CASCADE)
    file = models.FilePathField(path=os.path.join(settings.BASE_DIR, 'hdf5', 'debiased'), max_length=200, recursive=True)
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)

    def read_data(self, key):
        # print('Reading {} in {}'.format(key, self.file))
        # with pd.HDFStore(self.file, 'r') as store:
        #     return store[key]
        return pd.read_hdf(self.file, key)

    @property
    def rcp26(self):
        return self.read_data('rcp26')
    @property
    def rcp45(self):
        return self.read_data('rcp45')
    @property
    def rcp85(self):
        return self.read_data('rcp85')

    def data_aggreg(self, key, freq='Y'):
        group = self.read_data(key).resample(freq, kind='period')
        return {
            'sum': group.sum(min_count=1),
            'max': group.max(),
            'min': group.min(),
            'avg': group.mean(),
    }[self.param.agg]

    def __str__(self):
        return "{} {}".format(self.zone, self.param)

class Station(NamedModel):
    alt = models.IntegerField(null=True, verbose_name='altitude' ,default=0)
    STATION_TYPES = (('synoptic', 'synoptique'), ('climatic', 'climatique'), ('pluvio', 'pluviomètre'),)
    type = models.CharField(choices=STATION_TYPES, max_length=12, default='pluvio')
    coords = models.PointField(blank=True, null=True)
    lon = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)

    def stationproj_ordered(self):
        return self.stationproj_set.select_related('param').order_by('param__id')

    def params(self):
        pass

    def save(self, *args, **kwargs):
        if self.lon and self.lat:
            self.coords = Point(self.lon, self.lat)
        else:
            self.lon, self.lat = self.coords.coords
        super(Station, self).save(*args, **kwargs)

class StationParam(models.Model):
    """Replacement for StationData"""
    file = models.FilePathField(verbose_name='HDF5 storage file')
    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    param = models.ForeignKey(Parameter, on_delete=models.CASCADE)

    def read_rcp(self, key):
        return pd.read_hdf(self.file, key)
    # alias
    read_data = read_rcp

    def list_rcp(self):
        with pd.HDFStore(self.file, 'r') as store:
            return map(lambda x:x[1:], store) # remove '/' prefix

