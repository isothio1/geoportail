import json
import pandas as pd
import io
import base64

from django.http import JsonResponse, HttpResponse, HttpResponseRedirect, HttpRequest
from django.shortcuts import render, reverse
from django.core.serializers import serialize
from django.utils.translation import LANGUAGE_SESSION_KEY, get_language, gettext as _

from stats.decorators import count_proj_request
from . import models as pm
from .cst import *
from .reqscrapper import RequestScrapper
# HYDRO TEST
# from .hydro import image2base64


def image2base64(image):
    """PIL base64 rendering"""
    buf = io.BytesIO()
    image.save(buf, format='PNG', quality=100)
    code = base64.b64encode(buf.getvalue())
    return code.decode('utf-8')


# STATIC PAGES
def help(request):
    context = {
        'refyrs': DEFAULT_REFYRS
    }
    l = get_language()
    file = 'help_fr.html' if l and l.startswith('fr') else 'help.html'
    return render(request, 'proj/' + file, context)


def formula(request):
    l = get_language()
    file = 'formula_fr.html' if l and l.startswith('fr') else 'formula.html'
    return render(request, 'proj/' + file,
                  {'vars': ', '.join(['<b>{}</b> ({})'.format(v.shortname, v.locale_name)
                                      for v in pm.Parameter.objects.all()]),
                   'scenarios': pm.Scenario.objects.all(),
                   'params': pm.Parameter.objects.all(),
                   'zones': [pm.Zone.objects.get(name=settings.MAIN_ZONE)] + list(pm.Zone.objects.exclude(name=settings.MAIN_ZONE)),
                   })


def about(request):
    context = {
        'catmetric': pm.MetricCategory.objects.order_by('-name'),
        'models': pm.ClimateModel.objects.order_by('name'),
        'metrics': pm.Metric.objects.order_by('category', 'name'),
        'scenario': pm.Scenario.objects.order_by('name'),
    }
    return render(request, 'proj/about.html', context)


def faq(request):
    return render(request, 'proj/faq.html')

def download(request):
    context = {
        'scenarios': pm.Scenario.objects.all(),
        'params': pm.Parameter.objects.all(),
        'zones': [pm.Zone.objects.get(name=settings.MAIN_ZONE)] + list(pm.Zone.objects.exclude(name=settings.MAIN_ZONE)),
        'metrics': pm.Metric.objects.all(),
        'models': pm.ClimateModel.objects.order_by('name'),
    }
    return render(request, 'proj/download.html', context)


# LANG TOGGLE
def lang(request, l):
    request.session[LANGUAGE_SESSION_KEY] = l
    return HttpResponseRedirect(request.GET.get('next', reverse('proj:index')))

# MAIN PAGE
def index(request, simple=False):
    if 'lang' in request.GET:
        request.session[LANGUAGE_SESSION_KEY] = request.GET['lang']
        # reload the view
        return index(HttpRequest(), simple=simple)

    regions = pm.Zone.objects.exclude(name=settings.MAIN_ZONE)
    default_metric = pm.Metric.objects.get(shortname=settings.DEFAULT_METRIC).id
    graph_colors = {s.name: {'color': s.color} for s in pm.Scenario.objects.all()}
    graph_colors['ref'] = {'color': 'black'}
    graph_colors[_('model average')] = {'color': 'red', 'strokeWidth': 4, 'strokeBorderWidth': 3}
    graph_colors.update({s.name: {'id': s.id, 'color': s.color, 'strokePattern': DYGRAPH_STROKE.get(s.style, [])}
                         for s in pm.ClimateModel.objects.all()})
    places = pm.Place.objects.all()
    stations = pm.Station.objects.all()#.annotate(projs=Count('stationproj')).filter(projs__gt=0)

    # transform (xmin, xmax, ymin, ymax) into (xmin, ymin, xmax, ymax)
    view = settings.RASTER_BOX.split(',')
    view[1], view[2] = view[2], view[1]

    context = {
        'main_zone': settings.MAIN_ZONE,
        'stations': serialize('geojson', stations),
        'allstations': json.dumps(list(stations.values_list('name', flat=True))),
        'scenarii': pm.Scenario.objects.all(),
        'default_scenario': pm.Scenario.objects.get(name=request.GET.get('scen', DEFAULT_SCENARIO)),
        'regions': serialize('geojson', regions),
        'catmetric': pm.MetricCategory.objects.order_by('-name'),
        'default_metric': pm.Metric.objects.get(id=request.GET.get('metric', default_metric)),
        'allmetrics' : serialize('json', pm.Metric.objects.all()),
        'years': request.GET.get('yrs', DEFAULT_YRS),
        'default_refyrs': request.GET.get('refyrs', DEFAULT_REFYRS),
        'compare': request.GET.get('compare', 'scenarii'),
        'models': pm.ClimateModel.objects.all(),
        'graph_color': json.dumps(graph_colors),
        'places': serialize('geojson', places.filter(show=True)),
        'allplaces': serialize('geojson', places),
        'allplaces_list': json.dumps(list(places.values_list('name', flat=True))),
        'custom_cmap': pm.ColorMap.objects.filter(custom=True).order_by('name'),
        'view': ','.join(view),
    }
    if 'metric' in request.GET and 'yrs' in request.GET:
        context['args'] = '?' + '&'.join(['{}={}'.format(k,v) for (k, v) in request.GET.items()])
    if 'station' in request.GET:
        context['station'] = request.GET['station']

    # TODO : sync with classic
    template = 'proj/newsimple.html' if simple else 'proj/index.html'
    return render(request, template, context)


def simple(request):
    return index(request, simple=True)

def metric_download(request, met, scen, model=None):
    dflist = []
    for z in pm.Zone.objects.all():
        zd = pm.ZonedData.objects.get(zone=z, metric__shortname=met)
        df = zd.get_data(scen)
        if model :
            df = df[model]
        else:
            df = df.mean(axis=1)
        df.name = z.name
        dflist.append(df)
    total = pd.concat(dflist, axis=1)
    r = HttpResponse(content_type='text/csv')
    total.to_csv(r)
    return r

def zone_metric_download(request, met, scen, zone):
    zd = pm.ZonedData.objects.get(zone__name=zone, metric__shortname=met)
    df = zd.get_data(scen)
    r = HttpResponse(content_type='text/csv')
    df.to_csv(r)
    return r

def param_download(request, pr, scen, zone):
    drd = pm.DailyRegionData.objects.get(param__shortname=pr, zone__name=zone)
    df = drd.read_data(scen).round(2)
    r = HttpResponse(content_type='text/csv')
    df.to_csv(r)
    return r

def netcdf_download(request, metricid, modelid, scen):
    qset = pm.MonthlyRasterData.objects.filter(model__id=modelid, scenario__name=scen, metric__id=metricid)
    for mrd in qset:
        data = mrd.raster.bands[0].data()

benchmark = False
if benchmark:
    import time

@count_proj_request
def jsoner(request, update):
    """Main data view.

    update : string, contains one or more of
        'm' (map),
        's' (season, aka month),
        'y' (year).
    """

    if benchmark:
        start = time.time()

    rs = RequestScrapper(request)
    jres = {
        'zone': rs.real_zone,
    }

    if benchmark:
        now = time.time()
        print('rs : {}'.format(now-start))
        start = now

    if rs.metric:
        jres['metric'] = rs.metric.to_dict()
    else:
        jres['metric'] = {
            'name': rs.formula,
            'formula': rs.formula,
            'unit': '',
            'monthly': True,
        }
    if rs.postcalc == 'data':
        jres['metric']['plus'] = ''
    else:
        jres['metric']['plus'] = '+'
        if rs.postcalc == 'relative':
            jres['metric']['unit'] = '%'

    jres['metric']['raster'] = rs.has_raster()

    if benchmark:
        now = time.time()
        print('meta : {}'.format(now - start))
        start = now

    if 'm' in update:
        if rs.showraster or (rs.metric and rs.metric.raster_only):
            b, meta, scale = rs.get_raster()
            # meta = upperleftx | upperlefty | width | height | scalex | scaley | skewx | skewy | srid | numbands
            jres['raster'] = {
                'image': 'data:image/png;base64,' + image2base64(b),
                'extent': [round(meta[0], 2), round(meta[1] + meta[3] * meta[5], 2),
                           round(meta[0] + meta[2] * meta[4], 2), round(meta[1], 2)],
                'xres': round(meta[4],2),
                'yres': round(meta[5],2),
                'scale': scale,
            }
        else:
            rs.fill_regions_df()
            df, scale = rs.map_df()
            if df.empty:
                jres['map'] = False
            else:
                jres['map'] = {
                    'values': df.to_dict(orient='index'),
                    'scale': scale,
                    'title': rs.title['map'],
                }

    if benchmark:
        now = time.time()
        print('map : {}'.format(now - start))
        start = now


    if 's' in update or 'y' in update:
        rs.fill_loc_df()
        if 's' in update and rs.monthly:
            jres['month'] = rs.df_to_dygraphs(rs.month_df())
            if jres['month']:
                jres['month']['meta'].update({
                    'title': rs.title['month'],
                })

        if 'y' in update:
            jres['year'] = rs.df_to_dygraphs(rs.year_df())
            if jres['year']:
                jres['year']['meta'].update({
                    'showRangeSelector': True,
                    'title': rs.title['year'],
                })

        if rs.lonlat:
            v = rs.point_value()
            plus = jres['metric']['plus'] if v > 0 else ''
            jres['point'] = _('Selected pixel : {}{} {}').format(plus, v, jres['metric']['unit'])

    if benchmark:
        now = time.time()
        print('ys : {}'.format(now - start))
        start = now


    if rs.error:
        jres['error'] = '<ul>' + ''.join(['<li class="err' + e[1] + '">' + e + '</li>' for e in rs.error]) + '</ul>'
    jres['cScen'] = rs.compare_scen

    return JsonResponse(jres)


def text_export(request, period, extension):
    rs = RequestScrapper(request)
    rs.fill_loc_df()
    if period == 'year':
        df = rs.year_df()
    else:
        df = rs.month_df()
    if rs.metric:
        name = rs.metric.shortname + '_'
    else:
        name = ''

    if extension == 'csv':
        r = HttpResponse(content_type='text/csv')
        r['Content-Disposition'] = 'attachment; filename="{}{}.csv"'.format(name, rs.zone)
        df.to_csv(r)  # , header=False)
        return r

    # xls
    r = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    r['Content-Disposition'] = 'attachment; filename="{}{}.xls"'.format(name, rs.zone)
    writer = pd.ExcelWriter(r)
    df.to_excel(writer, 'Data')
    writer.save()
    return r


def csv_export(request, period):
    return text_export(request, period, 'csv')


def xls_export(request, period):
    return text_export(request, period, 'xls')
