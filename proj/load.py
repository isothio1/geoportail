import csv
import os
import pandas as pd
import subprocess # for cdo
from django.contrib.gis import geos
from django.contrib.gis.gdal import GDALRaster, DataSource
from django.contrib.gis.geos import GEOSGeometry, Point
from netCDF4 import Dataset, num2date
import numpy as np

from .cst import *
from . import models as pm
from .rastercutter import polygonize

HDF5DIR = os.path.join(settings.BASE_DIR, 'hdf5')

# GEO

def zones(shpfile=settings.ZONES_FILE, zonename='name', delete_existing=False):
    """Loads a shapefile into the database.

    zonename : the field of the shapefile containing the name of each vector feature."""
    if delete_existing:
        print('Removing existing zones.')
        pm.Zone.objects.all().delete()
    lyr = DataSource(shpfile)[0]
    for poly in lyr:
        if isinstance(poly.geom.geos, geos.MultiPolygon):
            mpoly = poly.geom.geos
        else:
            mpoly = geos.MultiPolygon(poly.geom.geos)
        try:
            # update zones (resolution, limit change) without modifying the whole database
            zone = pm.Zone.objects.get(name=poly.get(zonename))
            zone.mpoly = mpoly
        except pm.Zone.DoesNotExist:
            zone = pm.Zone(name=poly.get(zonename), mpoly=mpoly)
        print('Zone {} loaded.'.format(poly.get(zonename)))
        zone.save()


def places(csvfile=settings.PLACES_FILE, lonlat='wkt', delete_existing=False):
    """Loads a csv file with columns.

    If lonlat is 'wkt', the file has 3 columns :
        name, wkt, show on map Y/N
    where wkt is "POINT (lon, lat)"

    Otherwise, the file must have 4 columns :
        name, lon, lat, show on map Y/N
    """
    if delete_existing:
        pm.Place.objects.all().delete()
    with open(csvfile) as f:
        reader = csv.reader(f)
        reader.__next__()  # head
        plist = []
        oldplaces = pm.Place.objects.all().values_list('name', flat=True)
        if lonlat == 'wkt':
            for name, wkt, show in reader:
                if name not in oldplaces:
                    g = GEOSGeometry(wkt)
                    plist.append(pm.Place(lonlat=g, lon=g.coords[0], lat=g.coords[1], name=name, name_fr=name,
                                          show=(show == 'Y')))
        else:
            for name, lon, lat, show in reader:
                if name not in oldplaces:
                    plist.append(pm.Place(lonlat=Point(lon, lat), lon=lon, lat=lat, name=name, name_fr=name,
                                          show=(show == 'Y')))


    pm.Place.objects.bulk_create(plist)
    print("{} places created".format(len(plist)))


# HDF5


def zoneddata(hdfdir=HDF5DIR):
    """Reads a hdf5 directory for files of pattern 'zonedmetrics/metric.shortname/zone.h5'
    and stores the result into ZoneData."""
    bulk = []
    pm.ZonedData.objects.all().delete()
    for met in pm.Metric.objects.all():
        for z in pm.Zone.objects.all():
            file = os.path.join(hdfdir, 'zonedmetrics', met.shortname, z.name+'.h5')
            if os.path.exists(file):
                bulk.append(pm.ZonedData(metric=met, zone=z, file=file))
    pm.ZonedData.objects.bulk_create(bulk)
    return len(bulk)

def dailyregiondata(hdfdir=HDF5DIR):
    """Reads a hdf5 directory for files of pattern 'debiased/param.shortname/zone.h5'
    and stores the result into DailyRegionData."""
    bulk = []
    pm.DailyRegionData.objects.all().delete()
    for p in pm.Parameter.objects.all():
        for z in pm.Zone.objects.all():
            file = os.path.join(hdfdir, 'debiased', p.shortname, z.name+'.h5')
            if os.path.exists(file):
                bulk.append(pm.DailyRegionData(param=p, zone=z, file=file))
    pm.DailyRegionData.objects.bulk_create(bulk)
    return len(bulk)

# NETCDF

DATATYPES = {'int8': 1, 'uint16': 2, 'int16': 3, 'uint32': 4, 'int32': 5, 'float32': 6, 'float64': 7, }

class NCDFData:
    """Loads a netcdf file as numpy array. Used to output it with to_gdal() method."""

    def __init__(self, ncfile, param='none', lon='lon', lat='lat'):
        with Dataset(ncfile) as ds:
            if param not in ds.variables :
                # try and guess parameter name
                for v in ds.variables.keys():
                    if v.lower() not in [ 'time', 'lon', 'lat', 'latitude', 'longitude' ]:
                        param = v
                        break
            self.data = ds[param][:]

            if ds[lat][0].data < ds[lat][-1].data:
                # inverts latitude so that top is first
                # (necessary for correct display by postgis ST_AsPNG)
                self.data = np.flip(self.data, ds[param].dimensions.index(lat)).copy()

            self.width = ds[lon].shape[0]
            self.height = ds[lat].shape[0]
            self.lonres = round(abs(ds[lon][1]-ds[lon][0]), 4)
            self.latres = round(abs(ds[lat][1]-ds[lat][0]), 4)
            self.minlon = round(ds[lon][0].data-self.lonres/2, 4)
            self.maxlat = round(max(ds[lat])+self.latres/2, 4)
            self.datatype = DATATYPES[ ds[param].dtype.name ]
            self.dates = num2date(ds['time'][:], units=ds['time'].units, calendar=ds['time'].calendar)
            self.ntimes = ds['time'].shape[0]
            self.path = ncfile
            self.param = param

    def to_gdal(self, i):
        """GDAL raster of one layer (band) of a netcdf for postgis use."""

        # float conversion is mandatory, because GDALRaster checks type
        # and unfortunately np.float32 != float
        return GDALRaster({
            'srid': 4326,
            'width': self.width,
            'height': self.height,
            # 'geotransform': [self.minlon, self.lonres, 0, self.maxlat, -self.latres, 0],
            'origin': [ float(self.minlon), float(self.maxlat) ], # upperleft
            'scale': [ float(self.lonres), -float(self.latres) ], # latres is negative
            'datatype': self.datatype,
            'bands': [{
                'data' : self.data[i],
                'nodata_value': -9999#float(self.data.fill_value),
            }],
        })

def netcdf(metric, file_pattern='netcdf/{model}_{scenario}.nc', delete_existing=True):
    """MonthlyRasterData loader."""
    if delete_existing:
        pm.MonthlyRasterData.objects.filter(metric=metric).delete()
    n = 0
    for cm in pm.ClimateModel.objects.all():
        for scen in pm.Scenario.objects.all():
            file = file_pattern.format(model=cm.name, scenario=scen)
            if os.path.exists(file):
                print('Reading {}'.format(file))
                nc = NCDFData(file, metric.shortname)
                if metric.monthly:
                    bulk = [pm.MonthlyRasterData(model=cm, scenario=scen, metric=metric,
                                                 year=nc.dates[i].year, month=nc.dates[i].month, raster=nc.to_gdal(i))
                                                 # year=i // 12 + YEAR_RANGE[0], month=i % 12 + 1, raster=nc.to_gdal(i))
                            for i in range(nc.ntimes)]
                else:
                    bulk = [pm.MonthlyRasterData(model=cm, scenario=scen, metric=metric,
                                                 year=nc.dates[i].year, month=-1, raster=nc.to_gdal(i))
                            for i in range(TOTAL_YEARS)] # TODO
                n += len(bulk)
                pm.MonthlyRasterData.objects.bulk_create(bulk)
    print('Total {} rasters created.'.format(n))


from . import formula

def metric_from_formula(metric, hdfdir=HDF5DIR):
    """Calculates metric from parameter using metric.formula and metric.aggregation."""
    models = pm.ClimateModel.objects.all()
    context = formula.CONTEXT.copy()
    for z in pm.Zone.objects.all():
        print('Calculating for '+z.name)
        filename = os.path.join(hdfdir, z.name+'.h5')
        vars = formula.variables(metric.formula)
        drds = z.dailyregiondata_set.filter(param__shortname__in=vars)
        for s in pm.Scenario.objects.all():
            df = pd.DataFrame()
            pvars = {drd.param.shortname: drd.read_data(s.name) for drd in drds}
            for cm in models:
                try:
                    for v, vdata in pvars.items():
                        context[v] = vdata[cm.name]
                        df[cm.name] = eval(metric.formula, context)
                except KeyError:
                    continue
            # finally, aggregate
            gr = df.resample('M') if metric.monthly else df.groupby('Y')
            df = {
                'sum': gr.sum(),
                'mean': gr.mean(),
                'count': gr.count(),
                'min': gr.min(),
                'max': gr.max(),
            }[metric.aggregation]
            df.to_hdf(filename, s.name)
        pm.ZonedData(metric=metric, zone=z, file=filename).save()


def metric_from_nc(metric, file_pattern='netcdf/{model}_{scenario}.nc',
                         hdfdir=HDF5DIR, tmpdir='/tmp/', shpfile=settings.ZONES_FILE,
                         zoned_data=True, delete_existing=True, recut=False):
    """Loads rasters into postgis as MonthlyRasterData.
    If zoned_data==True, also vectorizes rasters according to given shapefile, then stores the result
    as hdf5 files and saves them as ZoneData.
    If recut==True, start by resizing every netcdf into settings.RASTER_BOX.

    args
    metric          : a proj.models.Metric instance
    file_pattern    : the pattern of netcdf files, including strings '{model}' and '{scenario}'
    hdfdir          : (optional) output directory
    tmpdir          : (optional) temporary directory, will be deleted on exit
    shpfile         : (optional) shapefile of regions
    zoned_data      : (optional, default True) also computes values for regions
    delete_existing : (optional, default True) removes previous data
    recut           : (optional, default False) starts by re-cutting to Senegal box, for faster execution
    """
    scenlist = pm.Scenario.objects.all()

    if recut:
        # checks that cdo exists
        if subprocess.run(['which', 'cdo']).returncode == 1:
            print('No cdo executable found. Recut disabled.')
            fp = file_pattern
            recut = False
        else:
            os.makedirs(os.path.join(tmpdir, metric.shortname), exist_ok=True)
            fp = os.path.join(tmpdir, metric.shortname, '{model}_{scenario}.nc')
    else:
        fp = file_pattern

    if zoned_data:
        print('Polygonizing netcdf.')
        zonelist = pm.Zone.objects.all()
        zones = {z.name: [] for z in zonelist}

    for scen in scenlist:
        n = 0
        for cm in pm.ClimateModel.objects.all():
            file = file_pattern.format(model=cm.name, scenario=scen.name)
            if os.path.exists(file):
                n += 1
                tmpnc = fp.format(model=cm.name, scenario=scen.name)
                if recut:
                    print('Cutting {} to box.'.format(file))
                    subprocess.run(['cdo', 'sellonlatbox,'+RASTER_BOX, file, tmpnc])
                if zoned_data :
                    df = polygonize(tmpnc, shpfile=shpfile, param=metric.shortname)
                    for z in df.columns:
                        s = df[z]
                        s.name = (scen.name, cm.name)
                        zones[z].append(s)
        if zoned_data:
            print('\n{} : {} models polygonized'.format(scen, n))

    if zoned_data:
        print('\nLoading regions.')
        reverse_zones = {z.name: z for z in zonelist}
        bulk = []
        if delete_existing:
            pm.ZonedData.objects.filter(metric=metric).delete()
        hdfdir = os.path.join(hdfdir, metric.shortname)
        os.makedirs(hdfdir, exist_ok=True)
        for z, l in zones.items():
            df = pd.concat(l, axis=1)
            file = os.path.join(hdfdir, z + '.h5')
            print('Saving HDF5 for {} in {}'.format(z, file))
            df.columns = pd.MultiIndex.from_tuples(df.columns)
            for scen in scenlist:
                try:
                    df[scen.name].to_hdf(file, scen.name)
                except OSError:
                    raise OSError('ERROR : file {} not writable.\n'
                                  'Check permissions for directory {}.\n'.format(file, hdfdir))
            bulk.append(pm.ZonedData(metric=metric, zone=reverse_zones[z], file=file))
        pm.ZonedData.objects.bulk_create(bulk)

    print('\nLoading rasters into postgis database.')
    netcdf(metric, file_pattern=fp, delete_existing=delete_existing)

def parameter_from_csv(param, file_pattern):
    """Stores daily region parameter from csv files.
    Files patterns must contain the '{scenario}' and '{zone}' pattern."""
    hdfdir = os.path.join(HDF5DIR, 'debiased', param.shortname)
    os.makedirs(hdfdir, exist_ok=True)
    for z in pm.Zone_objects.all():
        out = os.path.join(hdfdir, z.name + '.h5')
        if os.path.exists(out):
            os.remove(out)
        print('\nWriting {} :'.format(out), end='')

        with pd.HDFStore(out, 'w') as store:
            for s in pm.Scenario.objects.all():
                f = file_pattern.format(zone=z.name, scenario=s.name)
                if os.path.exists(f):
                    df = pd.read_csv(f, index_col=0, parse_dates=True)
                    print(s, end=' ')
                    store[s.name] = df
        pm.DailyRegionData(file=out, param=param, zone=z).save()


def parameter_from_nc(param, file_pattern, shpfile=settings.ZONES_FILE):
    """Cuts and store daily region parameter data based on a directory of netcdf files.
    Does not store raster data.

    args
    param        : proj.models.Parameter instance
    file_pattern : (string) netcdf file pattern containing '{model}' and '{scenario}' substring
    shpfile      : (optional) shapefile of regions
    """
    scenlist = pm.Scenario.objects.all()
    modlist = pm.ClimateModel.objects.all()
    total = []
    cols = []
    try:
        for s in scenlist:
            for mod in modlist:
                f = file_pattern.format(model=mod.name, scenario=s.name)
                if os.path.exists(f):
                    print('Loading '+f)
                    try:
                        df = polygonize(f, shpfile=shpfile, param=param.shortname, avoid_360=True)
                    except IndexError:
                        print('Aborted.')
                        continue
                    cols.append((s.name, mod.name))
                    total.append(df)
    except KeyboardInterrupt:
        pass # for debugging

    df = pd.concat(total, axis=1, keys=cols).reorder_levels([2,0,1], axis=1)
    hdfdir = os.path.join(HDF5DIR, 'debiased', param.shortname)
    os.makedirs(hdfdir, exist_ok=True)
    drds = []
    for z in pm.Zone.objects.all():
        if z.name in df:
            f = os.path.join(hdfdir, z.name+'.h5')
            print('Writing '+f)
            if os.path.exists(f):
                os.remove(f)
            with pd.HDFStore(f, 'w') as store:
                for s in scenlist:
                    store[s.name] = df[z.name][s.name]
        drds.append(pm.DailyRegionData(file=f, param=param, zone=z))
    print('Database registering {} files.'.format(len(drds)))
    pm.DailyRegionData.objects.bulk_create(drds)

def stations_from_csv(csvfile, lonlat=None):
    """If lonlat is 'wkt', the file has 2 or 3 columns :
        name, wkt [, optional : S/C/P for station type]
    where wkt is "POINT (lon, lat)"

    Otherwise, the file must have 3 or 4 columns :
        name, lon, lat [, optional : S/C/P for station type]
    """
    types = { 'S': 'synoptic', 'C': 'climatic', 'P':'pluvio' }
    with open(csvfile) as f:
        reader = csv.reader(f)
        reader.__next__()  # head
        plist = []
        oldplaces = pm.Station.objects.all().values_list('name', flat=True)
        if lonlat == 'wkt':
            for row in reader:
                name, wkt = row[:2]
                type = types[row[2]] if len(row) == 3 else 'pluvio'
                if name not in oldplaces:
                    print(name)
                    g = GEOSGeometry(wkt)
                    plist.append(pm.Station(coords=g, lon=g.coords[0], lat=g.coords[1], name=name, name_fr=name, type=type))
        else:
            for row in reader:
                name, lon, lat = row[:3]
                type = types[row[3]] if len(row) == 4 else 'pluvio'
                if name not in oldplaces:
                    print(name)
                    plist.append(pm.Station(coords=Point(float(lon), float(lat)), lon=lon, lat=lat, name=name, name_fr=name, type=type))
    pm.Station.objects.bulk_create(plist)
    print("{} stations created".format(len(plist)))


def station_projs_from_csv(csvdir, shortparam, delete_existing=True, station_name=None):
    """Loads a series of csv projections.

    If station_name is provided, only loads a specific station."""
    hdfdir = os.path.join(HDF5DIR, 'stationparam', shortparam)
    os.makedirs(hdfdir, exist_ok=True)
    for s in pm.Scenario.objects.all():
        d = os.path.join(csvdir, s.name)
        for f in os.scandir(d):
            base, ext = os.path.splitext(f.name)
            if ext != '.csv' or (station_name and base != station_name):
                continue
            try:
                station = pm.Station.objects.get(name=base)
                param = pm.Parameter.objects.get(shortname=shortparam)
            except (pm.Station.DoesNotExist, pm.Parameter.DoesNotExist):
                continue
            try:
                sp = pm.StationParam.objects.get(param=param, station=station)
            except pm.StationParam.DoesNotExist:
                sp = pm.StationParam(param=param, station=station, file=os.path.join(hdfdir, station.name + '.h5'))
                sp.save()
            print('Loading {}'.format(f.path))
            if delete_existing and sp.file and os.path.exists(sp.file):
                os.remove(sp.file)
            df = pd.read_csv(f.path, index_col=0, parse_dates=True)
            df.to_hdf(sp.file, s.name)
        # delete only once !
        delete_existing = False


# save
def stations_to_csv(csvfile):
    with open(csvfile, 'w') as f:
        w = csv.writer(f)
        for s in pm.Station.objects.all() :
            w.writerow((s.name, s.lon, s.lat, s.type[0].capitalize()))

def stationparams_to_csv(csvdir):
    for station in pm.Station.objects.all():
        for sp in station.stationparam_set.all():
            for s in pm.Scenario.objects.all():
                path = os.path.join(csvdir, sp.param.shortname, s.name)
                os.makedirs(path, exist_ok=True)
                csvfile = os.path.join(path, station.name + '.csv')
                print(csvfile)
                df = sp.read_rcp(s.name)
                df.to_csv(csvfile)

