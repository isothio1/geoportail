from django.contrib.gis.db import models
from django.db.models import Aggregate, Func
from django.db.models.expressions import RawSQL

# EX :
# vsplus = vs.annotate(raster=RUnion('yearlyvalue__raster', filter=Q(yearlyvalue__year__range=(2000, 2005))))

class RUnion(Aggregate):
    """Raster aggregation. See  http://www.postgis.net/docs/ST_Union.html
    method is one of 'MEAN' (default), 'LAST', 'FIRST', 'MIN', 'MAX', 'COUNT', 'SUM', 'RANGE'.
    """
    function = 'ST_Union'
    output_field = models.RasterField()
    template = "%(function)s(%(expressions)s, '%(method)s')"

    def __init__(self, expression, method='MEAN', **extra):
        super().__init__(expression, method=method, **extra)


class AsMem(Func):
    """GDAL Memory Data Source."""
    function = 'ST_AsGDALRaster'
    template = "%(function)s(%(expressions)s, 'Tiff')"
    output_field = models.FloatField()


class NoData(Func):
    """Nodata value."""
    function = 'ST_BandNoDataValue'
    template = '%(function)s(%(expressions)s)'
    output_field = models.FloatField()

class RStats(Func):
    """Base class for raster stats.
    See https://postgis.net/docs/RT_ST_SummaryStats.html"""
    function = 'ST_SummaryStats'
    output_field = models.FloatField()

class RMax(RStats):
    template = "(%(function)s(%(expressions)s)).max"
class RMin(RStats):
    template = "(%(function)s(%(expressions)s)).min"
class RAvg(RStats):
    template = "(%(function)s(%(expressions)s)).mean"
class RStd(RStats):
    template = "(%(function)s(%(expressions)s)).stddev"


class Value(Func):
    output_field = models.FloatField()
    function = 'ST_Value'
    arity = 2

# EX :
# yv.annotate(poly=zone_poly('Senegal')).annotate(clip=Clip('raster', 'poly'))

class Clip(Func):
    function = 'ST_Clip'
    arity = 2
    output_field = models.RasterField()


class Algebra(Func):
    function = 'ST_MapAlgebra'
    arity = 2
    output_field = models.RasterField()
    template = "%(function)s((%expressions)s, '%(formula)s')"


# DEPRECATED (use GDALRaster() instead)
class Dump(Func):
    function = 'ST_DumpValues'
    template = '%(function)s(%(expressions)s, 1)'


# NOT WORKING YET

class Png(Func):
    function = 'ST_AsPNG'
    output_field = models.BinaryField()
    template = "%(function)s(ST_Colormap(%(expressions)s, '%(colormap)s'))"


# TEMPORARY CLASS
# qs = m.MonthlyValue.objects.filter(value_set__metric__name='total rainfall').annotate(sen=SenegalClip('raster'))

class SenegalClip(Func):
    function = 'ST_Clip'
    output_field = models.RasterField()
    arity = 1
    template = "%(function)s(%(expressions)s, ST_MakeEnvelope(-19,11,-10.5,18,4326))"
