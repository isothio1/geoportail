from django.urls import path, register_converter
from . import views, export, basic

class Extension:
    regex = '[a-z]{2,3}'

    def to_python(self, value):
        return str(value)

    to_url = to_python

register_converter(Extension, 'xxx')


app_name = 'climap'

urlpatterns = [
    path('main/', views.main, name='main'),
    path('pre/', views.pre, name='pre'),
    path('img.png', views.image, name='image'),
    path('pinpoint/', views.point, name='pinpoint'),
    path('scale/<int:Metric_id>.png', views.scale, name='scale'),
    path('metadata/<int:Metric_id>/', views.metadata, name='metadata'),
    path('zones/<int:div_id>', views.zones, name='zones'),
#    path('plot/<int:Metric_id>', views.chartplot, name='plot'),
#    path('plot/<int:Metric_id>/<xxx:extension>', views.chartplot, name='csv'),
    path('help/', views.help, name='help'),
    path('api/', views.api, name='api'),
    path('month.<xxx:extension>', views.month, name='month'),
    path('year.<xxx:extension>', views.year, name='year'),
    path('basic', basic.index, name='basic'),
    path('export.<xxx:extension>', basic.image, name='plt'),
    path('data/', export.data, name='data'),
    path('download/', export.index, name='download'),
    path('confidence/', views.conf, name='confidence'),
    path('feedback/', basic.feedback, name='feedback'),
    path('feedbacksubmit/', basic.feedbacksubmit, name='feedbacksubmit'),
    path('guide/', basic.guide, name='guide'),
    path('', views.newindex, name='index'),
]