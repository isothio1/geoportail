from django.contrib.gis.db import models
import numpy as np
from .load import run_yearlyvalueset, run_monthlyvalueset, scaleraster, run_polygons
from django.db import connection
from .rasterfuns import RMax, RMin
from math import floor, ceil
from django.db.models import Max, Min

class NamedModel(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)
        abstract = True


# class Attribution(NamedModel):
#     file = models.FileField(upload_to='attributions/', blank=True)
#     url = models.URLField(blank=True)

class Division(NamedModel):
    file = models.FileField(upload_to='tmp/', blank=True)
    zonename = models.CharField(max_length=20, blank=True, help_text='Column in the shapefile for the actual name of the zone.')

    def save(self, *args, **kwargs):
        super(Division, self).save(*args, **kwargs)
        if(self.file):
            self.zone_set.all().delete()
            run_polygons(self, self.zonename)
            self.file.delete()


class Zone(NamedModel):
    division = models.ForeignKey(Division, on_delete=models.CASCADE)
    mpoly = models.MultiPolygonField()
    iso_code = models.CharField(max_length=4, blank=True)


class ModelGroup(NamedModel):
    color = models.CharField(max_length=20, help_text='Line color in graphs.')

class ClimateModel(NamedModel):
    description = models.TextField(blank=True, max_length = 500)
    url = models.URLField(blank=True)
    color = models.CharField(max_length=20, help_text='Line color in graphs.')
    STYLE_CHOICES = ('solid', 'dashed', 'dotted', 'dashdot')
    style = models.CharField(choices=zip(STYLE_CHOICES, STYLE_CHOICES), max_length=max(map(len,STYLE_CHOICES)))
    yearmin = models.IntegerField()
    yearmax = models.IntegerField()
    group = models.ForeignKey(ModelGroup, null=True, on_delete=models.SET_NULL)


class Scenario(NamedModel):
    color = models.CharField(max_length=20, help_text='Line color in graphs.')


class Colormap(NamedModel):
    """Coloring schemes for producing png."""
    code = models.TextField(max_length=300, help_text="List of rows of the form '{}:R:G:B:A'. Each row represents "
                           "a value linearly spaced between vmax and vmin. The last row "
                           "should always be 'nv:0:0:0:0' to allow transparency. "
                           "See <a href='http://colorbrewer2.org'>Colorbrewer</a> for ideas.)")

    def code2colormap(self):
        nums = list(map(lambda x:x.split(','), self.code.split('\r\n')))
        rgb = ['red', 'green', 'blue']
        cmap = {}
        n = len(nums)
        for c in range(3):
            coco = []
            for x in range(n):
                val = int(nums[x][c + 1])/255
                coco.append((1-(x/(n-1)), val, val))
            coco.reverse()
            cmap[rgb[c]] = coco
        return cmap


class MetricCategory(NamedModel):
    public = models.BooleanField(default=False, blank=True)

    class Meta:
        verbose_name_plural = "Metric categories"

    def get_ordered_metrics(self):
        """For display in templates."""
        return self.metric_set.filter(caseof__isnull=True)


class Metric(NamedModel):
    class Meta:
        ordering = ('shortname', 'name')

    unit = models.CharField(max_length=20)
    shortname = models.CharField(max_length=20, help_text="For exporting files.")
    yearvmin = models.FloatField(default=0)
    yearvmax = models.FloatField(default=0)
    monthvmin = models.FloatField(default=0)
    monthvmax = models.FloatField(default=0)
    scaleraster = models.RasterField(null=True)
    description = models.TextField(blank=True)
    colormap = models.ForeignKey(Colormap, null=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(MetricCategory, null=True, on_delete=models.CASCADE)
    has_month = models.BooleanField(default=True, blank=True)
    caseof = models.ForeignKey('self', null=True, on_delete=models.SET_NULL, blank=True)

    def get_min_max(self, period='year'):
        if period == 'year':
            yv = YearlyValue.objects.filter(value_set__metric_id=self.id)\
                .annotate(rmax=RMax('raster'), rmin=RMin('raster'))
            self.yearvmin = floor(yv.aggregate(Min('rmin'))['rmin__min'])
            self.yearvmax = ceil(yv.aggregate(Max('rmax'))['rmax__max'])
        else:
            mv = MonthlyValue.objects.filter(value_set__metric_id=self.id)\
                .annotate(rmax=RMax('raster'), rmin=RMin('raster'))
            self.monthvmin = floor(mv.aggregate(Min('rmin'))['rmin__min'])
            self.monthvmax = ceil(mv.aggregate(Max('rmax'))['rmax__max'])
        self.save()

    # def extract_minmax(self, minmax='max', period='year'):
    #     with connection.cursor() as cursor:
    #         cursor.execute('select {minmax}(res) from (select (st_summarystats(raster)).{minmax} as res '
    #                        'from climap_{period}lyvalue, climap_valueset where value_set_id = climap_valueset.id '
    #                        'and metric_id = %s) as foo;'.format(minmax=minmax, period=period), [self.id])
    #         return cursor.fetchone()[0]

    def color_code(self, relative=False, vmin=None, vmax=None, period='year'):
        if relative:
            code = self.colormap.code
            vals = [ str(int(i))+'%%' for i in np.linspace(100, 0, code.count('{')) ]
        else:
            if period == 'year':
                vmin = vmin or self.yearvmin
                vmax = vmax or self.yearvmax
            else:
                vmin = vmin or self.monthvmin
                vmax = vmax or self.monthvmax

            lines = self.colormap.code.split('\n')
            code = '\n'.join([lines[0].format('100%%')] + lines + [lines[-2].format('0%%')])
            vals = np.linspace(vmax, vmin, code.count('{'))
        return self.colormap.code.format(*vals)

    def save(self, *args, **kwargs):
        self.scaleraster = scaleraster(self.yearvmin, self.yearvmax)
        super(Metric, self).save(*args, **kwargs)


class ValueSet(models.Model):
    model = models.ForeignKey(ClimateModel, null=True, on_delete=models.CASCADE)
    scenario = models.ForeignKey(Scenario, null=True, on_delete=models.CASCADE)
    metric = models.ForeignKey(Metric, on_delete=models.CASCADE)
    year_file = models.FileField(upload_to='tmp/', blank=True)
    month_file = models.FileField(upload_to='tmp/', blank=True)
    var = models.CharField(max_length=20, blank=True,
                           help_text='Variable used in the file. If unsure, '
                                     'try "cdo showname filename.nc" in commande line.')

    def __str__(self):
        return '{} {}, {}'.format(self.model, self.scenario, self.metric)


class YearlyValue(models.Model):
    class Meta:
        indexes = [
            models.Index(fields=['value_set']),
            models.Index(fields=['year']),
        ]
    year = models.IntegerField()
    raster = models.RasterField()
    value_set = models.ForeignKey(ValueSet, on_delete=models.CASCADE)

    def __str__(self):
        return '{}, year {}'.format(self.value_set, self.year)

months = ( 'January', 'February', 'March', 'April', 'May', 'June', 'July',
           'August', 'September', 'October', 'November', 'December' )
shortmonths = [ m[:3] for m in months ]

class MonthlyValue(models.Model):
    class Meta:
        indexes = [
            models.Index(fields=['value_set']),
            models.Index(fields=['year']),
            models.Index(fields=['month']),
        ]

    year = models.IntegerField()
    month = models.IntegerField()
    raster  = models.RasterField()
    value_set = models.ForeignKey(ValueSet, on_delete=models.CASCADE)

    def strmonth(self):
        return months[self.month]

    def __str__(self):
        return '{}, {} {}'.format(self.value_set, months[self.month], self.year)


class FeedBack(models.Model):
    date = models.DateTimeField(auto_now=True)
    #ip = models.GenericIPAddressField()
    usage = models.TextField(blank=True)
    missing = models.TextField(blank=True)
    download = models.TextField(blank=True)
    interface = models.TextField(blank=True)
    suggest = models.TextField(blank=True)
    def __str__(self):
        return 'Feedback on {}'.format(self.date)
