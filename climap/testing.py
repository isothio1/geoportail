from . import models
from climap import models as m
from django.db import connection

params = {
    'yrend': 2060,
    'metric_id': m.Metric.objects.get(name__contains='rainfall').id,
    'scenario_id': 1,
    'zone': 'Senegal'
}
model_list = [ mod.id for mod in m.ClimateModel.objects.all() ]
vs_sc = m.ValueSet.objects.filter(model__id__in=model_list, metric_id=params['metric_id'])
vs_cm = vs_sc.filter(scenario__name='rcp85')

CM_NAME = 'value_set__model__name'
SC_NAME = 'value_set__scenario__name'


def get_vsids():
    metric = models.Metric.objects.first()
    scen = models.Scenario.objects.first()
    vsids = []
    for m in models.ClimateModel.objects.all():
        try:
            vsids.append(models.ValueSet.objects.get(metric=metric,scenario=scen,model=m))
        except models.ValueSet.DoesNotExist:
            pass
    return vsids


def get_png(yrval):
    with connection.cursor() as cursor:
        cursor.execute("SELECT ST_AsPNG(ST_Reclass(raster, '%s-%s:1-250', '8BUI')) from climap_yearlyvalue limit 1;",
                       [yrval.metric.vmin, yrval.metric.vmax])
        png = cursor.fetchone()[0]
    with open('tmp.png', 'bw') as f:
        f.write(png)


def get_tiff(yr):
    with connection.cursor() as cursor:
        cursor.execute('select ST_AsTiff(raster) from climap_yearlyvalue where id = %s;', [yr.id])
        tiff = cursor.fetchone()[0]
    with open('tmp.tiff', 'bw') as f:
        f.write(tiff)


def clip_country(c):
    with connection.cursor() as cursor:
        cursor.execute("select ST_AsPNG(ST_Reclass(ST_Clip(raster, mpoly), 1,"
                       "'14-50:1-255', '8BUI', ST_BandNoDataValue(raster))) "
                       "from climap_zone, climap_yearlyvalue " 
                       "where name = %s and metric_id = 1 and year = 2018;", [c])
        png = cursor.fetchone()[0]
        with open(c+'.png', 'bw') as f:
            f.write(png)


def clip_country_tiff(c):
    with connection.cursor() as cursor:
        cursor.execute("select ST_AsTIFF(ST_Clip(raster, mpoly)) "
                       "from climap_zone, climap_yearlyvalue " 
                       "where name = %s and metric_id = 1 and year = 2018;", [c])
        tiff = cursor.fetchone()[0]
        with open(c+'.tiff', 'bw') as f:
            f.write(tiff)


# def get_colored_png():
#     with connection.cursor() as cursor:
#         cursor.execute("SELECT ST_AsPNG(ST_ColorMap(raster,'310,35,75,100,0\
# 300,10,65,95,0\
# 290,0,45,70,0\
# 280,0,26,40,0\
# 268,0,7,10,0\
# nv,255,255,255')) from climap_dailyvalue limit 1;")
#         png = cursor.fetchone()[0]
#     return png
def get_colored_png():
    with connection.cursor() as cursor:
        cursor.execute("SELECT ST_AsPNG(ST_ColorMap(raster,'grayscale')) from climap_dailyvalue limit 1;")
        png = cursor.fetchone()[0]
    return png


def write_png():
    with open('tmp.png', 'bw') as f:
        f.write(get_colored_png())

import matplotlib.pyplot as plt

def imshow_nowhite(X):
    fig = plt.figure(figsize=(15,16), dpi=10, frameon=False)
    ax = plt.axes([0,0,1,1])
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.axis('off')
    plt.imshow(X, aspect='equal', interpolation='none', origin='lower', cmap='hot')
    fig.savefig('bla.png', bbox_inches='tight', pad_inches=0)


def getminmax(var, period=1, vmin = 0, vmax = 0):
    for t in range(period):
        for x in range(var.shape[1]):
            for y in range(var.shape[2]):
                vmin = min(vmin, var[t][x, y])
                vmax = max(vmax, var[t][x, y])
    return vmin, vmax
