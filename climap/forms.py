from django import forms
from climap import models as m
from django.db.models import Count

SCALE_CHOICES = (('relative', 'relative'), ('absolute','absolute'))
METHOD_CHOICES = (('mean', 'average'), ('max', 'max'), ('min','min'), ('sum','accumulation'))
CHANGE_CHOICES = ((None, '-------'), ('absolute', 'absolute'), ('relative','relative (anomaly)'))
MONTHS_CHOICES = [(-1, 'all year'),] + [(i, m.months[i]) for i in range(len(m.months))]
COMPARE_CHOICES = (('scenarii', 'scenarii'), ('m', 'm'), ('groups', 'model groups'))
IMG_FORMATS = ('png', 'pdf', 'svg', 'eps')

senegal = m.Zone.objects.get(name='Senegal')
countries = m.Division.objects.get(name='countries')

class MapForm(forms.Form):
    zone = forms.ModelChoiceField(m.Zone.objects.filter(division=countries).order_by('name'), label='Zone',
                                  initial=senegal,
                                  )
                                  # required=False, help_text='<br>Can be un-set for a larger view.')
    parameter = forms.ModelChoiceField(m.Metric.objects.filter(caseof__isnull=True).order_by('category', 'shortname'), empty_label=None,
                                       help_text='<a href="../climap/help/">more on parameters</a>')
    scenario = forms.ModelChoiceField(m.Scenario.objects.all().annotate(vs=Count('valueset')).filter(vs__gt=0), empty_label=None)
    climate_model = forms.ModelMultipleChoiceField(m.ClimateModel.objects.order_by('name'),
                                                   label='Climate m',
                                                   help_text='more on <a href="../climap/help/#m">m</a>, <a href="../climap/help/#groups">model groups</a>'
                                                             '<br>Several m can be selected.',
                                                   initial=m.ClimateModel.objects.all())

    month = forms.ChoiceField(choices=MONTHS_CHOICES)
    yr = forms.IntegerField(initial=2050, label='Start year')
    yrend = forms.IntegerField(initial=2050, label='End year')
    method = forms.ChoiceField(choices=METHOD_CHOICES,
                               help_text='<br>Aggregation method for periods longer than one year.')

    change = forms.ChoiceField(choices=CHANGE_CHOICES,
                               required=False,
                               help_text='<br>By default, displays raw values.')
    refYrs = forms.CharField(initial='1950-1980', required=False, label='Reference period')
    # scale = forms.ChoiceField(choices=SCALE_CHOICES,
    #                           help_text='<br>Absolute scale uses fixed bounds relevant for the whole continent.')
    display_map = forms.BooleanField(initial=True, required=False)
    output = forms.ChoiceField(choices=(('pixel', 'pixel'), ('contour', 'contour')),
                               label='Map display')
    display_month = forms.BooleanField(initial=True, required=False, label='Display month by month time chart')
    display_year = forms.BooleanField(initial=True, required=False, label='Display yearly evolution time chart')
    compare = forms.ChoiceField(choices=COMPARE_CHOICES, label='Charts compare')
    smoothing = forms.IntegerField(initial=10, max_value=50, label="Year smoothing",
                                   help_text='<br>For model global comparison only.')
    format = forms.ChoiceField(choices=zip(IMG_FORMATS, IMG_FORMATS), label='Output format')
    dpi = forms.IntegerField(initial=100,help_text='%<br>For png only.',min_value=10, max_value=500, label='Image size')


class PublicMapForm(MapForm):
    parameter = forms.ModelChoiceField(m.Metric.objects.filter(caseof__isnull=True, category__public=True).order_by('category', 'shortname'), empty_label=None,
                                       help_text='<a href="../climap/help/">more on parameters</a>')

PERIOD = ('month', 'year')
EXPORT_FORMATS = ('netcdf', 'csv')

class DownloadForm(forms.Form):
    zone = forms.ModelChoiceField(m.Zone.objects.filter(division=countries).order_by('name'),
                                  required=False,
                                  initial=senegal,
                                  label='Zone', empty_label='All',
                                  help_text="<br>Returns a rectangle containing selected zone.")
    parameter = forms.ModelChoiceField(m.Metric.objects.order_by('category', 'name'), empty_label=None,
                                       help_text='<br><a href="../climap/help/">description of parameters</a>')
    climate_model = forms.ModelChoiceField(m.ClimateModel.objects.order_by('name'),
                                           label='Climate model', empty_label='(average)', required=False,
                                           help_text='<br><a href="../climap/help/#m">description of m</a>')
    scenario = forms.ModelChoiceField(m.Scenario.objects.exclude(name='reference').order_by('name'), empty_label=None)
    period = forms.ChoiceField(choices = zip(PERIOD, PERIOD), initial='year', label="Time step")
    yr = forms.IntegerField(initial=1950, min_value=1950, max_value=2099, label='Start year')
    yrend = forms.IntegerField(initial=2099, min_value=1950, max_value=2099, label='End year')
    format = forms.ChoiceField(choices = zip(EXPORT_FORMATS, EXPORT_FORMATS))


class PublicDownloadForm(DownloadForm):
    parameter = forms.ModelChoiceField(m.Metric.objects.filter(category__public=True).order_by('category', 'name'), empty_label=None,
                                       help_text='<br><a href="../climap/help/">description of parameters</a>')


# class FeedBack(forms.Form):
#     usage = forms.CharField(widget=forms.Textarea, required=False)
#     missing = forms.CharField(widget=forms.Textarea, required=False)
#     download = forms.CharField(widget=forms.Textarea, required=False)
#     interface = forms.CharField(widget=forms.Textarea, required=False)
#     suggest = forms.CharField(widget=forms.Textarea, required=False)

class FeedBack(forms.ModelForm):
    class Meta:
        model=m.FeedBack
        fields=['usage','missing','download','interface','suggest']