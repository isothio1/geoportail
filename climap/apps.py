from django.apps import AppConfig


class ClimapConfig(AppConfig):
    name = 'climap'
