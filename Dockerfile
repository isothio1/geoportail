# https://github.com/thinkWhere/GDAL-Docker/blob/develop/3.6/Dockerfile

FROM python:3.6-slim

# Set the working directory to /app
WORKDIR /app

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y libgdal-dev g++ cdo # nco
# Update C env vars so compiler can find gdal
ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal

# Copy the current directory contents into the container at /app
COPY . /app/

# install numpy BEFORE gdal
RUN pip install numpy
RUN pip install GDAL==2.2.3
# Install other packages
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Define environment variable
# PYTHONUNBUFFERED ensures our console output looks familiar and is not buffered by Docker, which we don’t want.
ENV PYTHONUNBUFFERED 1
