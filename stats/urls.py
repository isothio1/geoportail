from django.urls import path
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.views.generic import TemplateView

from . import views
app_name = 'stats'

urlpatterns = [
    path('login', LoginView.as_view(), name='login'),
    path('doc', TemplateView.as_view(template_name='stats/doc.html'), name='doc'),
    path('metric/<ip>', views.proj_metric, name='metric_by_ip'),
    path('metric', views.proj_metric, name='metric'),
    path('metriccsv', views.proj_metric_csv, name='metric_csv'),
    path('comments', login_required(views.CommentListView.as_view()), name='comments'),
    path('commentwrite', views.CommentCreate.as_view(), name='commentwrite'),
    path('commentdel/<int:pk>', login_required(views.CommentDelete.as_view()), name='commentdel'),
    path('', views.index, name='index'),
]