
from .models import ProjMetric, HistoMetric

def count_proj_request(func):
    def wrapper(request, *args, **kwargs):
        ProjMetric(url=request.META.get('QUERY_STRING'),  #request.build_absolute_uri()[:200],
                   nav=request.META.get('HTTP_USER_AGENT', '')[:100],
                   logged=request.user.is_authenticated,
                   ip=request.META.get('REMOTE_ADDR', '').split(':')[0],
                   ).save()
        return func(request, *args, **kwargs)
    return wrapper


def count_histo_request(func):
    def wrapper(request, *args, **kwargs):
        if 'sid' in kwargs:
            sid = kwargs['sid']
        else:
            sid = None
        HistoMetric(ip=request.META.get('REMOTE_ADDR', '').split(':')[0],
                    nav=request.META.get('HTTP_USER_AGENT', '')[:100],
                    station_id=sid,
                    logged=request.user.is_authenticated,
                    ).save()
        return func(request, sid=sid)
    return wrapper