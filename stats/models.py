from django.db import models

# Create your models here.

# SITE METRICS

class Metric(models.Model):
    """Abstract metric class."""
    class Meta:
        abstract = True
    date = models.DateTimeField(auto_now_add=True)
    ip = models.GenericIPAddressField(null=True)
    nav = models.CharField(max_length=100)


class ProjMetric(Metric):
    url = models.CharField(max_length=400)
    logged = models.BooleanField()

    def url_as_list(self):
        return ', '.join(self.url.split('&'))

class HistoMetric(Metric):
    station_id = models.IntegerField(null=True)
    logged = models.BooleanField()

# COMMENTS

class Comment(models.Model):
    name = models.CharField(max_length=100, default='anonymous')
    mail = models.EmailField(max_length=50, blank=True, help_text='not mandatory')
    comment = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} comment ({})'.format(self.name, self.date.strftime('%d/%m/%y'))